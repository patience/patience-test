#include "SolitaireTest.h"

#include <QtTest>

#include "../Patience/Card.h"
#include "../Patience/Solitaire.h"
#include "../Patience/CardStacksModel.h"
#include "../Patience/CardListModel.h"
#include "ModelTest.h"
#include "ModelObserver.h"

SolitaireTest::SolitaireTest(QObject *parent) :
    QObject(parent)
{
}

void SolitaireTest::newgameTest()
{
    Solitaire solitaire;
    solitaire.newGame();

    for (int i = 0; i < 7; i++)
        QCOMPARE(solitaire.solitaireModel()->stackCardsCount(i), i + 1);

    solitaire.drawCards();
    QVERIFY(solitaire.discardModel()->data(
                solitaire.discardModel()->index(solitaire.discardModel()->rowCount() - 1, 0),
                CardModel::SelectableRole).toBool());
}

void SolitaireTest::drawDeckTest()
{
    Solitaire solitaire;
    solitaire.newGame();

    for (int i = 0; i < 8; i++) {
        solitaire.drawCards();
        QCOMPARE(solitaire.discardModel()->rowCount(), 3);
    }

    solitaire.drawCards();
    QCOMPARE(solitaire.discardModel()->rowCount(), 0);
    QCOMPARE(solitaire.deckModel()->cardList().first()->isFlipped(), true);
    QCOMPARE(solitaire.deckModel()->cardList().last()->isFlipped(), true);
}

//flipped == visible
Solitaire* SolitaireTest::buildSolitaire()
{
    Solitaire* solitaire = new Solitaire(this);

    solitaire->deckModel()->pushCard(new Card(Card::Spades, Card::Queen));
    solitaire->deckModel()->pushCard(new Card(Card::Spades, Card::King));
    solitaire->deckModel()->pushCard(new Card(Card::Diamonds, Card::Ace));
    solitaire->drawCards();

    solitaire->acesModel()->pushCard(1, new Card(Card::Spades, Card::Jack));

    solitaire->solitaireModel()->pushCard(1, new Card(Card::Hearts, Card::Ace, false, this));

    solitaire->solitaireModel()->pushCard(2, new Card(Card::Hearts, Card::Five, true, this));
    solitaire->solitaireModel()->pushCard(2, new Card(Card::Hearts, Card::King, false, this));

    solitaire->solitaireModel()->pushCard(3, new Card(Card::Hearts, Card::Ten, true, this));
    solitaire->solitaireModel()->pushCard(3, new Card(Card::Spades, Card::Nine, false, this));
    solitaire->solitaireModel()->pushCard(3, new Card(Card::Hearts, Card::Eight, false, this));

    solitaire->solitaireModel()->pushCard(4, new Card(Card::Clubs, Card::Nine, true, this));
    solitaire->solitaireModel()->pushCard(4, new Card(Card::Diamonds, Card::Eight, false, this));
    solitaire->solitaireModel()->pushCard(4, new Card(Card::Spades, Card::Seven, false, this));
    solitaire->solitaireModel()->pushCard(4, new Card(Card::Hearts, Card::Six, false, this));

    solitaire->solitaireModel()->pushCard(5, new Card(Card::Spades, Card::Five, true, this));
    solitaire->solitaireModel()->pushCard(5, new Card(Card::Hearts, Card::Four, true, this));
    solitaire->solitaireModel()->pushCard(5, new Card(Card::Hearts, Card::Three, true, this));
    solitaire->solitaireModel()->pushCard(5, new Card(Card::Hearts, Card::Two, true, this));
    solitaire->solitaireModel()->pushCard(5, new Card(Card::Hearts, Card::Ace, false, this));

    solitaire->solitaireModel()->pushCard(6, new Card(Card::Spades, Card::Five, true, this));
    solitaire->solitaireModel()->pushCard(6, new Card(Card::Hearts, Card::Four, true, this));
    solitaire->solitaireModel()->pushCard(6, new Card(Card::Clubs, Card::Five, false, this));

    return solitaire;
}

void SolitaireTest::solitaireMoveTest()
{
    Solitaire* solitaire = buildSolitaire();
    QScopedPointer<ModelTest> testmodel(new ModelTest(solitaire->solitaireModel(), this));
    Q_UNUSED(testmodel);
    QScopedPointer<ModelObserver> observerModel(new ModelObserver(solitaire->solitaireModel(), Qt::UserRole, CardModel::LastRole));

    solitaire->solitaireMove(2, 0);     // Move the King in empty place
    QCOMPARE(solitaire->solitaireModel()->stackCardsCount(0), 1);
    QCOMPARE(solitaire->solitaireModel()->stackCardsCount(2), 1);
    QVERIFY(!solitaire->solitaireModel()->getCard(2, Card::Hearts, Card::Five)->isFlipped());
    QVERIFY(solitaire->solitaireModel()->getCard(2, Card::Hearts, Card::Five)->isSelectable());
    observerModel->checkSynchronised();

    solitaire->solitaireMove(4, 3);     // Move two cards
    QCOMPARE(solitaire->solitaireModel()->stackCardsCount(3), 5);
    QCOMPARE(solitaire->solitaireModel()->stackCardsCount(4), 2);
    QCOMPARE(solitaire->solitaireModel()->cardList(3).last()->getSuit(), Card::Hearts);
    QCOMPARE(solitaire->solitaireModel()->cardList(3).last()->getValue(), Card::Six);
    observerModel->checkSynchronised();

    solitaire->solitaireMove(2, 3);     // Illegal move
    QCOMPARE(solitaire->solitaireModel()->stackCardsCount(2), 1);
    QCOMPARE(solitaire->solitaireModel()->stackCardsCount(3), 5);
    observerModel->checkSynchronised();

    solitaire->solitaireMove(6, 3);     // move one card
    QCOMPARE(solitaire->solitaireModel()->stackCardsCount(6), 2);
    QCOMPARE(solitaire->solitaireModel()->stackCardsCount(3), 6);
    observerModel->checkSynchronised();

    delete solitaire;
}

void SolitaireTest::discardToSolitaireTest()
{
    Solitaire* solitaire = buildSolitaire();

    QCOMPARE(solitaire->discardModel()->rowCount(), 3);

    solitaire->discardToSolitaire(1);
    QCOMPARE(solitaire->discardModel()->rowCount(), 3);
    QVERIFY(solitaire->discardModel()->data(
                solitaire->discardModel()->index(solitaire->discardModel()->rowCount() - 1, 0),
                CardModel::SelectableRole).toBool());

    solitaire->discardToSolitaire(2);
    QCOMPARE(solitaire->discardModel()->rowCount(), 2);
    QCOMPARE(solitaire->solitaireModel()->stackCardsCount(2), 3);
    QVERIFY(solitaire->discardModel()->data(
                solitaire->discardModel()->index(solitaire->discardModel()->rowCount() - 1, 0),
                CardModel::SelectableRole).toBool());

    solitaire->discardToSolitaire(0);
    QCOMPARE(solitaire->discardModel()->rowCount(), 1);
    QCOMPARE(solitaire->solitaireModel()->stackCardsCount(0), 1);
    QVERIFY(solitaire->discardModel()->data(
                solitaire->discardModel()->index(solitaire->discardModel()->rowCount() - 1, 0),
                CardModel::SelectableRole).toBool());

    delete solitaire;
}

void SolitaireTest::discardToAcesTest()
{
    Solitaire* solitaire = buildSolitaire();
    QCOMPARE(solitaire->discardModel()->rowCount(), 3);

    solitaire->discardToAces(1);
    QCOMPARE(solitaire->discardModel()->rowCount(), 2);
    QCOMPARE(solitaire->acesModel()->stackCardsCount(1), 2);

    solitaire->discardToAces(1);
    QCOMPARE(solitaire->discardModel()->rowCount(), 1);
    QCOMPARE(solitaire->acesModel()->stackCardsCount(1), 3);

    solitaire->discardToAces();
    QCOMPARE(solitaire->discardModel()->rowCount(), 0);
    QCOMPARE(solitaire->acesModel()->stackCardsCount(0), 1);

    delete solitaire;
}

void SolitaireTest::solitaireToAcesTest()
{
    Solitaire* solitaire = buildSolitaire();

    solitaire->solitaireToAces(1, 2);
    QCOMPARE(solitaire->solitaireModel()->stackCardsCount(1), 0);
    QCOMPARE(solitaire->acesModel()->stackCardsCount(2), 1);

    solitaire->solitaireToAces(2, 3);
    QCOMPARE(solitaire->solitaireModel()->stackCardsCount(2), 2);
    QCOMPARE(solitaire->acesModel()->stackCardsCount(3), 0);

    solitaire->solitaireToAces(5);
    QCOMPARE(solitaire->solitaireModel()->stackCardsCount(5), 4);
    QVERIFY(!solitaire->solitaireModel()->cardList(5).last()->isFlipped());
    QVERIFY(solitaire->solitaireModel()->cardList(5).last()->isSelectable());

    delete solitaire;
}
