#ifndef CARDTEST_H
#define CARDTEST_H

#include <QObject>

class Card;

class CardTest : public QObject
{
    Q_OBJECT

public:
    CardTest();

private Q_SLOTS:
    void init();
    void cleanup();
    void invalidBuild();
    void redColorBuild();
    void blackColorBuild();
    void flippedBuild();
    void flipCard();
    void selectableBuild();
    void setFlippedCard();
    void setSelectable();

private:
    Card* tenDiamonds;
    Card* tenSpades;
};

#endif // CARDTEST_H
