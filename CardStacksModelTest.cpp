#include "CardStacksModelTest.h"

#include <QtTest/QtTest>
#include <QSignalSpy>

#include "../Patience/Card.h"
#include "../Patience/CardStacksModel.h"
#include "ModelTest.h"
#include "ModelObserver.h"

CardStacksModelTest::CardStacksModelTest(QObject *parent) :
    QObject(parent)
{
    qRegisterMetaType<QModelIndex>("QModelIndex");
}

void CardStacksModelTest::emptyList() {
    CardStacksModel model(3);
    QVERIFY(model.isEmpty(0));
    QVERIFY(model.isEmpty(1));
    QVERIFY(model.isEmpty(2));
    QCOMPARE(model.stacksCount(), 3);
    QScopedPointer<ModelObserver> observerModel(new ModelObserver(&model, CardStacksModel::SuitRole, CardStacksModel::ColorRole));

    model.pushCards(0, QList<Card*>() << new Card() << new Card() << new Card());
    QVERIFY(!model.isEmpty(0));
    QVERIFY(!model.isEmpty(-1));
    QVERIFY(!model.isEmpty(3));
    observerModel->checkSynchronised();
}

void CardStacksModelTest::pushCard() {
    CardStacksModel model(3);
    QScopedPointer<ModelTest> testmodel(new ModelTest(&model, this));
    Q_UNUSED(testmodel);
    QScopedPointer<ModelObserver> observerModel(new ModelObserver(&model, CardStacksModel::SuitRole, CardStacksModel::ColorRole));

    model.pushCard(1, new Card());

    QCOMPARE(model.stackCardsCount(1), 1);
    observerModel->checkSynchronised();
}

void CardStacksModelTest::pushCards() {
    CardStacksModel model(3);
    QScopedPointer<ModelTest> testmodel(new ModelTest(&model, this));
    Q_UNUSED(testmodel);
    QScopedPointer<ModelObserver> observerModel(new ModelObserver(&model, CardStacksModel::SuitRole, CardStacksModel::ColorRole));

    model.pushCards(1, QList<Card*>() << new Card() << new Card() << new Card());

    QCOMPARE(model.stackCardsCount(1), 3);
    observerModel->checkSynchronised();
}

void CardStacksModelTest::popCard() {
    CardStacksModel model(3);
    QScopedPointer<ModelTest> testmodel(new ModelTest(&model, this));
    Q_UNUSED(testmodel);
    QScopedPointer<ModelObserver> observerModel(new ModelObserver(&model, CardStacksModel::SuitRole, CardStacksModel::ColorRole));

    model.pushCard(1, new Card());
    observerModel->checkSynchronised();
    Card* card = model.popCard(1);

    QVERIFY(card);
    QCOMPARE(model.stackCardsCount(1), 0);
    observerModel->checkSynchronised();
}

void CardStacksModelTest::popLasts_data() {
    QTest::addColumn<int>("popCount");
    QTest::newRow("Zero") << 0;
    QTest::newRow("One") << 1;
    QTest::newRow("Three") << 3;
    QTest::newRow("Hundred") << 100;
}

void CardStacksModelTest::popLasts() {
    QFETCH(int, popCount);

    CardStacksModel model(3);
    QScopedPointer<ModelTest> testmodel(new ModelTest(&model, this));
    Q_UNUSED(testmodel);
    QScopedPointer<ModelObserver> observerModel(new ModelObserver(&model, CardStacksModel::SuitRole, CardStacksModel::ColorRole));

    addCardsToModel(&model, 1);
    int modelStartCount = model.stackCardsCount(1);
    observerModel->checkSynchronised();

    QList<Card*> card = model.popLasts(1, popCount);
    popCount = qMin(modelStartCount, popCount);

    if (popCount > 0) {
        QCOMPARE(card.last()->getSuit(), Card::Diamonds);
        QCOMPARE(card.last()->getValue(), Card::Seven);
    }

    QCOMPARE(card.count(), popCount);
    QCOMPARE(model.stackCardsCount(1), modelStartCount - popCount);
    observerModel->checkSynchronised();
}

void CardStacksModelTest::addCardsToModel(CardStacksModel *model, int stackIndex) {
    model->pushCard(stackIndex, new Card(Card::Diamonds, Card::Four));
    model->pushCard(stackIndex, new Card(Card::Clubs, Card::Ace));
    model->pushCard(stackIndex, new Card(Card::Spades, Card::King));
    model->pushCard(stackIndex, new Card(Card::Hearts, Card::Ten));
    model->pushCard(stackIndex, new Card(Card::Diamonds, Card::Seven));
}

void CardStacksModelTest::takeAll() {
    CardStacksModel model(3);
    addCardsToModel(&model, 1);
    int modelStartCount = model.stackCardsCount(1);
    QScopedPointer<ModelTest> testmodel(new ModelTest(&model, this));
    Q_UNUSED(testmodel);
    QScopedPointer<ModelObserver> observerModel(new ModelObserver(&model, CardStacksModel::SuitRole, CardStacksModel::ColorRole));

    QList<Card*> list = model.takeAll(1);

    QCOMPARE(model.stackCardsCount(1), 0);
    QCOMPARE(list.count(), modelStartCount);
    observerModel->checkSynchronised();
}

void CardStacksModelTest::clear() {
    CardStacksModel model(3);
    addCardsToModel(&model, 1);
    model.clearStack(1);
    QCOMPARE(model.stackCardsCount(1), 0);
}

void CardStacksModelTest::clearAll() {
    CardStacksModel model(3);
    addCardsToModel(&model, 0);
    addCardsToModel(&model, 1);
    addCardsToModel(&model, 2);
    model.clearAllStacks();
    QCOMPARE(model.stackCardsCount(0), 0);
    QCOMPARE(model.stackCardsCount(1), 0);
    QCOMPARE(model.stackCardsCount(2), 0);
}

void CardStacksModelTest::hasCardEmpty() {
    CardStacksModel model(3);
    QVERIFY(model.contains(1, Card::Diamonds, Card::Ace) == false);
}

void CardStacksModelTest::hasCardValid() {
    CardStacksModel model(3);
    addCardsToModel(&model, 1);
    model.pushCard(1, new Card(Card::Diamonds, Card::Queen));
    QVERIFY(model.contains(1, Card::Diamonds, Card::Queen));
}

void CardStacksModelTest::hasNotCard() {
    CardStacksModel model(3);
    model.pushCard(1, new Card(Card::Diamonds, Card::Queen));
    QVERIFY(model.contains(1, Card::Diamonds, Card::King) == false);
}

void CardStacksModelTest::getCard() {
    CardStacksModel model(3);
    Card* original = new Card(Card::Diamonds, Card::Queen);
    model.pushCard(1, original);
    const Card* invalidCard = model.getCard(1, Card::Spades, Card::Jack);
    const Card* validCard = model.getCard(1, Card::Diamonds, Card::Queen);

    QVERIFY(invalidCard == 00);
    QVERIFY(validCard != 00);
    QVERIFY(validCard->isValid());
    QVERIFY(validCard == original);
    QCOMPARE(validCard->getSuit(), original->getSuit());
    QCOMPARE(validCard->getValue(), validCard->getValue());
}

void CardStacksModelTest::setSelectable() {
    CardStacksModel model(3);
    QScopedPointer<ModelTest> testmodel(new ModelTest(&model, this));
    Q_UNUSED(testmodel);
    QScopedPointer<ModelObserver> observerModel(new ModelObserver(&model, CardStacksModel::SuitRole, CardStacksModel::ColorRole));

    Card* original = new Card(Card::Diamonds, Card::Queen);
    model.pushCard(1, original);
    model.pushCard(1, new Card(Card::Hearts, Card::Jack));
    model.setCardSelectable(1, original, true);
    model.setCardSelectable(1, Card::Hearts, Card::Jack, true);
    model.setCardSelectable(1, Card::Spades, Card::Ace, true);
    const Card* card1 = model.getCard(1, Card::Diamonds, Card::Queen);
    const Card* card2 = model.getCard(1, Card::Hearts, Card::Jack);

    QVERIFY(card1->isSelectable());
    QVERIFY(card2->isSelectable());
    observerModel->checkSynchronised();

    model.setCardSelectable(1, original, false);
    model.setCardSelectable(1, Card::Hearts, Card::Jack, false);
    QVERIFY(!card1->isSelectable());
    QVERIFY(!card2->isSelectable());
    observerModel->checkSynchronised();
}

void CardStacksModelTest::setSelectableAlreadySelected() {
    CardStacksModel model(3);
    Card* original = new Card(Card::Diamonds, Card::Queen);
    model.pushCard(1, original);
    model.setCardSelectable(1, original, true);
    QSignalSpy dataChangedSpy(&model, SIGNAL(dataChanged(QModelIndex,QModelIndex)));
    model.setCardSelectable(1, original, true);

    QCOMPARE(dataChangedSpy.count(), 0);
}

void CardStacksModelTest::setFlipped() {
    CardStacksModel model(3);
    QScopedPointer<ModelTest> testmodel(new ModelTest(&model, this));
    Q_UNUSED(testmodel);
    QScopedPointer<ModelObserver> observerModel(new ModelObserver(&model, CardStacksModel::SuitRole, CardStacksModel::ColorRole));

    Card* original = new Card(Card::Diamonds, Card::Queen);
    model.pushCard(1, original);
    model.pushCard(1, new Card(Card::Hearts, Card::Jack));
    model.setCardFlipped(1, original, true);
    model.setCardFlipped(1, Card::Hearts, Card::Jack, true);
    model.setCardFlipped(1, Card::Spades, Card::Ace, true);
    const Card* card1 = model.getCard(1, Card::Diamonds, Card::Queen);
    const Card* card2 = model.getCard(1, Card::Hearts, Card::Jack);

    QVERIFY(card1->isFlipped());
    QVERIFY(card2->isFlipped());
    observerModel->checkSynchronised();

    model.setCardFlipped(1, original, false);
    model.setCardFlipped(1, Card::Hearts, Card::Jack, false);
    QVERIFY(!card1->isFlipped());
    QVERIFY(!card2->isFlipped());
    observerModel->checkSynchronised();
}

void CardStacksModelTest::setFlippedAlreadyFlipped() {
    CardStacksModel model(3);
    Card* original = new Card(Card::Diamonds, Card::Queen);
    model.pushCard(1, original);
    model.setCardFlipped(1, original, true);
    QSignalSpy dataChangedSpy(&model, SIGNAL(dataChanged(QModelIndex,QModelIndex)));
    model.setCardFlipped(1, original, true);

    QCOMPARE(dataChangedSpy.count(), 0);
}

void CardStacksModelTest::dataTest() {
    CardStacksModel model(3);
    Card* original = new Card(Card::Diamonds, Card::Queen);
    model.pushCard(1, original);
    QCOMPARE((int)original->getValue(), model.data(model.index(0, 0, model.index(1, 0)), CardStacksModel::ValueRole).toInt());
    QCOMPARE((int)original->getSuit(), model.data(model.index(0, 0, model.index(1, 0)), CardStacksModel::SuitRole).toInt());
    QVERIFY(original->isFlipped() == model.data(model.index(0, 0, model.index(1, 0)), CardStacksModel::FlippedRole).toBool());
    QVERIFY(original->isSelectable() == model.data(model.index(0, 0, model.index(1, 0)), CardStacksModel::SelectableRole).toBool());
    QCOMPARE((int)original->getColor(), model.data(model.index(0, 0, model.index(1, 0)), CardStacksModel::ColorRole).toInt());
}

void CardStacksModelTest::setAllSelectable() {
    CardStacksModel model(3);
    addCardsToModel(&model, 1);
    Card* original = new Card(Card::Diamonds, Card::Queen);
    original->setSelectable(true);
    model.pushCard(1, original);
    model.setAllSelectable(1, true);
    QVERIFY(model.data(model.index(0, 0, model.index(1, 0)), CardStacksModel::SelectableRole).toBool() == true);
    QVERIFY(model.data(model.index(model.stackCardsCount(1) / 2, 0, model.index(1, 0)), CardStacksModel::SelectableRole).toBool() == true);
    QVERIFY(model.data(model.index(model.stackCardsCount(1) - 1, 0, model.index(1, 0)), CardStacksModel::SelectableRole).toBool() == true);
}

void CardStacksModelTest::setAllFlipped() {
    CardStacksModel model(3);
    addCardsToModel(&model, 1);
    Card* original = new Card(Card::Diamonds, Card::Queen);
    original->flip();
    model.pushCard(1, original);
    model.setAllFlipped(1, true);
    QVERIFY(model.data(model.index(0, 0, model.index(1, 0)), CardStacksModel::FlippedRole).toBool() == true);
    QVERIFY(model.data(model.index(model.stackCardsCount(1) / 2, 0, model.index(1, 0)), CardStacksModel::FlippedRole).toBool() == true);
    QVERIFY(model.data(model.index(model.stackCardsCount(1) - 1, 0, model.index(1, 0)), CardStacksModel::FlippedRole).toBool() == true);
}

void CardStacksModelTest::constCardList() {
    CardStacksModel model(3);
    model.pushCards(1, QList<Card*>() << new Card(Card::Diamonds, Card::Ten) <<
                       new Card(Card::Spades, Card::Queen) << new Card(Card::Diamonds, Card::King));
    QList<const Card*> constList = model.cardList(1);

    QCOMPARE(constList.size(), model.stackCardsCount(1));
    QCOMPARE(constList.first()->getValue(), Card::Ten);
    QCOMPARE(constList.first()->getSuit(), Card::Diamonds);
    QCOMPARE(constList.last()->getValue(), Card::King);
    QCOMPARE(constList.last()->getSuit(), Card::Diamonds);
}

void CardStacksModelTest::emptyStacks()
{
    CardStacksModel model(3);
    QCOMPARE(model.emptyStacksCount(), 3);
    addCardsToModel(&model, 1);
    QCOMPARE(model.emptyStacksCount(), 2);
    addCardsToModel(&model, 0);
    QCOMPARE(model.emptyStacksCount(), 1);
    addCardsToModel(&model, 2);
    QCOMPARE(model.emptyStacksCount(), 0);
}

