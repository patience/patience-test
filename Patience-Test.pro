#-------------------------------------------------
#
# Project created by QtCreator 2011-06-20T21:46:41
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = CardTest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += CardTest.cpp \
    ../Patience/Card.cpp \
    CardListModelTest.cpp \
    ../Patience/CardListModel.cpp \
    mainTest.cpp \
    CardStacksModelTest.cpp \
    ../Patience/CardStacksModel.cpp \
    SolitaireTest.cpp \
    ../Patience/Solitaire.cpp \
    ../Patience/NLastProxyModel.cpp \
    NLastProxyModelTest.cpp \
    ModelTest.cpp \
    ModelObserver.cpp \
    ../Patience/CardModel.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS += \
    ../Patience/Card.h \
    ../Patience/CardListModel.h \
    CardTest.h \
    CardListModelTest.h \
    CardStacksModelTest.h \
    ../Patience/CardStacksModel.h \
    SolitaireTest.h \
    ../Patience/Solitaire.h \
    ../Patience/NLastProxyModel.h \
    NLastProxyModelTest.h \
    ModelTest.h \
    ModelObserver.h \
    ../Patience/CardModel.h
