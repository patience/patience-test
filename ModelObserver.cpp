#include "ModelObserver.h"

#include <QTest>
#include <QVector>

/**
  \brief Connect a model to the observer, specify the roles range to be checked.
  The observer stay tuned like a view to the model, it copy and update its internal info based on model signals.
  After connection, test some functions that change the model data and then check the model.
  It will query against the model and check each data role for each node of the model.
  \todo Implement model moves
  */


class ObservedItem
{
public:
    ObservedItem(ObservedItem *parent = 0) :
        parentItem(parent),
        rowCount(0),
        columnCount(0)
    {
    }

    ~ObservedItem() {
        for (int row = 0; row < rowCount; row++) qDeleteAll(childItems[row]);
    }

    void setChildrenSize(int rowCount, int columnCount) {
        this->rowCount = rowCount;
        this->columnCount = columnCount;
        childItems.resize(rowCount);
        for (int row = 0; row < rowCount; row++) childItems[row].resize(columnCount);
    }

    void setChild(ObservedItem *child, int row, int column = 0) {
        Q_ASSERT(row >= 0 && row < rowCount);
        Q_ASSERT(column >= 0 && column < columnCount);
        Q_ASSERT(childItems.size() == rowCount);
        Q_ASSERT(childItems[row].size() == columnCount);

        childItems[row][column] = child;
    }

    ObservedItem *child(int row, int column = 0) {
        Q_ASSERT(row >= 0 && row < rowCount);
        Q_ASSERT(column >= 0 && column < columnCount);
        Q_ASSERT(childItems.size() == rowCount);
        Q_ASSERT(childItems[row].size() == columnCount);

        return childItems[row][column];
    }

    void insertRows(int row, int count = 1) {
        Q_ASSERT(row >= 0 && row <= rowCount);
        childItems.insert(row, count, QVector<ObservedItem*>(columnCount, 00));
        rowCount += count;
    }

    void insertColumns(int column, int count = 1) {
        Q_ASSERT(column >= 0 && column <= columnCount);
        for (int row = 0; row < rowCount; row++) childItems[row].insert(column, count, 00);
        columnCount += count;
    }

    void removeRows(int row, int count = 1) {
        Q_ASSERT(row >= 0 && row + count <= rowCount);

        for (int rowDelete = row; rowDelete < row + count; rowDelete++)
            qDeleteAll(childItems[rowDelete]);

        childItems.remove(row, count);
        rowCount -= count;
    }

    void removeColumns(int column, int count = 1) {
        Q_ASSERT(column >= 0 && column <= columnCount);

        for (int row = 0; row < rowCount; row++) {
            for (int columnDelete = column; columnDelete < column + count; columnDelete++) {
                delete childItems[row][columnDelete];
            }
            childItems[row].remove(column, count);
        }
        columnCount -= count;
    }

    int getRowCount() const {
        return this->rowCount;
    }

    int getColumnCount() const {
        return this->columnCount;
    }

    ObservedItem *parent() {
        return parentItem;
    }

    QList<QVariant>& dataList() {
        return dataRoles;
    }

private:
    QVector< QVector<ObservedItem*> > childItems;
    int rowCount;
    int columnCount;
    ObservedItem *parentItem;

    QList< QVariant > dataRoles;
};

ModelObserver::ModelObserver(QObject *parent) :
    QObject(parent),
    firstRole(0),
    lastRole(0),
    model(00),
    rootItem(00)
{
}

ModelObserver::ModelObserver(QAbstractItemModel *model, int firstRole, int lastRole, QObject *parent) :
    QObject(parent),
    firstRole(0),
    lastRole(0),
    model(00),
    rootItem(00)
{
    setModel(model, firstRole, lastRole);
}

void ModelObserver::setModel(QAbstractItemModel *model, int firstRole, int lastRole)
{
    this->model = model;
    this->firstRole = firstRole;
    this->lastRole = lastRole;
    syncToModel();

    connect(model, SIGNAL(rowsInserted(const QModelIndex&, int, int)),
            this, SLOT(onRowsInserted(QModelIndex,int,int)));
    connect(model, SIGNAL(rowsRemoved(const QModelIndex&, int, int)),
            this, SLOT(onRowsRemoved(QModelIndex,int,int)));
    connect(model, SIGNAL(modelReset()), this, SLOT(onModelReset()));
    connect(model, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(onDataChanged(QModelIndex,QModelIndex)));
    connect(model, SIGNAL(columnsInserted(const QModelIndex&, int, int)),
            this, SLOT(onColumnsInserted(QModelIndex,int,int)));
    connect(model, SIGNAL(columnsRemoved(const QModelIndex&, int, int)),
            this, SLOT(onColumnsRemoved(QModelIndex,int,int)));
}

void ModelObserver::syncToModel()
{
    clearInternalTree();
    Q_ASSERT(rootItem == 00);

    rootItem = new ObservedItem();

    loadNodeData(QModelIndex(), rootItem);
}

void ModelObserver::clearInternalTree() {
    if (rootItem != 00) delete rootItem;
    rootItem = 00;
}

// This function load RECURSIVELY each item in the model at modelIndex position into the observer item
void ModelObserver::loadNodeData(const QModelIndex &modelIndex, ObservedItem *observer) {
    int rowCount = model->rowCount(modelIndex);
    int columnCount = model->columnCount(modelIndex);
    Q_ASSERT(observer != 00);

    loadRoleData(modelIndex, observer);

    // recursively load children data
    observer->setChildrenSize(rowCount, columnCount);
    for (int row = 0; row < rowCount; row++) {
        for (int column = 0; column < columnCount; column++) {
            ObservedItem* item = new ObservedItem(observer);
            observer->setChild(item, row, column);
            loadNodeData(model->index(row, column, modelIndex), item);
        }
    }
}

void ModelObserver::loadRoleData(const QModelIndex &modelIndex, ObservedItem *observer) {
    observer->dataList().clear();

    for (int role = firstRole; role <= lastRole; role++) {
        observer->dataList().append(model->data(modelIndex, role));
    }
}

void ModelObserver::checkSynchronised() const
{
    checkNodeData(QModelIndex(), rootItem);
    return;
}

// Recursively check all data roles for each child items
void ModelObserver::checkNodeData(const QModelIndex &modelIndex, ObservedItem *observer) const
{
    int rowCount = model->rowCount(modelIndex);
    int columnCount = model->columnCount(modelIndex);
    Q_ASSERT(observer != 00);
    QCOMPARE(observer->getRowCount(), rowCount);
    QCOMPARE(observer->getColumnCount(), columnCount);

    // Check data roles
    QCOMPARE(observer->dataList().size(), lastRole - firstRole + 1);
    for (int role = firstRole; role <= lastRole; role++) {
        QCOMPARE(observer->dataList().value(role - firstRole), model->data(modelIndex, role));
    }

    // Recursively check children
    for (int row = 0; row < rowCount; row++) {
        for (int column = 0; column < columnCount; column++) {
            ObservedItem* item = observer->child(row, column);
            checkNodeData(model->index(row, column, modelIndex), item);
        }
    }
}

void ModelObserver::onModelReset() {
    syncToModel();
}

void ModelObserver::onRowsInserted(QModelIndex parent, int start, int end)
{
    ObservedItem* parentItem = getItemFromIndex(parent);
    Q_ASSERT(parentItem !=00);
    parentItem->insertRows(start, end - start + 1);

    // Load data for these new nodes
    for (int row = start; row <= end; row++) {
        for (int column = 0; column < parentItem->getColumnCount(); column++) {
            ObservedItem* item = new ObservedItem(parentItem);
            parentItem->setChild(item, row, column);
            loadNodeData(model->index(row, column, parent), item);
        }
    }
}

ObservedItem * ModelObserver::getItemFromIndex(const QModelIndex &modelIndex)
{
    if (!modelIndex.isValid()) return rootItem;
    else return getItemFromIndex(modelIndex.parent())->child(modelIndex.row(), modelIndex.column());
}

void ModelObserver::onRowsRemoved(QModelIndex parent, int start, int end)
{
    ObservedItem* parentItem = getItemFromIndex(parent);
    Q_ASSERT(parentItem !=00);
    parentItem->removeRows(start, end - start + 1);
}

void ModelObserver::onDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight)
{
    Q_ASSERT(topLeft.parent() == bottomRight.parent());

    ObservedItem* parentItem = getItemFromIndex(topLeft.parent());
    Q_ASSERT(parentItem !=00);

    for (int row = topLeft.row(); row <= bottomRight.row(); row++) {
        for (int column = topLeft.column(); column <= bottomRight.column(); column++) {
            loadRoleData(model->index(row, column, topLeft.parent()),
                         parentItem->child(row, column));
        }
    }
}

void ModelObserver::onColumnsInserted(QModelIndex parent, int start, int end)
{
    ObservedItem* parentItem = getItemFromIndex(parent);
    Q_ASSERT(parentItem !=00);
    parentItem->insertColumns(start, end - start + 1);

    // Load data for these new nodes
    for (int row = 0; row < parentItem->getRowCount(); row++) {
        for (int column = start; column < start - end + 1; column++) {
            ObservedItem* item = new ObservedItem(parentItem);
            parentItem->setChild(item, row, column);
            loadNodeData(model->index(row, column, parent), item);
        }
    }
}

void ModelObserver::onColumnsRemoved(QModelIndex parent, int start, int end)
{
    ObservedItem* parentItem = getItemFromIndex(parent);
    Q_ASSERT(parentItem !=00);
    parentItem->removeColumns(start, end - start + 1);
}
