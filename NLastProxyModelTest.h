#ifndef NLASTPROXYMODELTEST_H
#define NLASTPROXYMODELTEST_H

#include <QObject>

class NLastProxyModelTest : public QObject
{
    Q_OBJECT
public:
    explicit NLastProxyModelTest(QObject *parent = 0);

private slots:
    void staticDataTest();
    void pushDataTest();
    void popDataTest();
    void clearDataTest();
    void dataModifiedTest();

};

#endif // NLASTPROXYMODELTEST_H
