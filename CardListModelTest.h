#ifndef CARDLISTMODELTEST_H
#define CARDLISTMODELTEST_H

#include <QObject>

class CardListModel;

class CardListModelTest : public QObject
{
    Q_OBJECT

public:
    CardListModelTest();

private Q_SLOTS:
    void emptyList();
    void pushCard();
    void pushCards();
    void popCard();
    void buildDeck32();
    void buildDeck52();
    void popLasts_data();
    void popLasts();
    void takeAll();
    void clear();
    void hasCardEmpty();
    void hasCardValid();
    void hasNotCard();
    void getCard();
    void setSelectable();
    void setSelectableAlreadySelected();
    void setFlipped();
    void setFlippedAlreadyFlipped();
    void dataTest();
    void setAllFlipped();
    void setAllSelectable();
    void constCardList();

private:
    CardListModel* buildDeckModel();
};

#endif // CARDLISTMODELTEST_H
