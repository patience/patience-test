#ifndef MODELOBSERVER_H
#define MODELOBSERVER_H

#include <QObject>
#include <QAbstractItemModel>

class ObservedItem;

class ModelObserver : public QObject
{
    Q_OBJECT
public:
    explicit ModelObserver(QObject *parent = 0);
    explicit ModelObserver(QAbstractItemModel* model, int firstRole, int lastRole, QObject* parent = 0);

    void setModel(QAbstractItemModel* model, int firstRole, int lastRole);
    void checkSynchronised() const;

private slots:
    void onRowsInserted(QModelIndex parent, int start, int end);
    void onRowsRemoved(QModelIndex parent, int start, int end);
    void onColumnsInserted(QModelIndex parent, int start, int end);
    void onColumnsRemoved(QModelIndex parent, int start, int end);
    void onModelReset();
    void onDataChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight);

private:
    void syncToModel();
    void clearInternalTree();
    void loadNodeData(const QModelIndex& modelIndex, ObservedItem* observer);
    void loadRoleData(const QModelIndex& modelIndex, ObservedItem* observer);
    void checkNodeData(const QModelIndex& modelIndex, ObservedItem* observer) const;
    ObservedItem* getItemFromIndex(const QModelIndex& modelIndex);

private:
    QAbstractItemModel* model;
    int firstRole;
    int lastRole;
    ObservedItem* rootItem;
};

#endif // MODELOBSERVER_H
