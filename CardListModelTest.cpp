#include "CardListModelTest.h"

#include <QtCore/QString>
#include <QtTest/QtTest>
#include <QSignalSpy>

#include "../Patience/Card.h"
#include "../Patience/CardListModel.h"
#include "ModelTest.h"
#include "ModelObserver.h"

CardListModel* CardListModelTest::buildDeckModel() {
    CardListModel* model = new CardListModel();
    model->pushCard(new Card(Card::Diamonds, Card::Four));
    model->pushCard(new Card(Card::Clubs, Card::Ace));
    model->pushCard(new Card(Card::Spades, Card::King));
    model->pushCard(new Card(Card::Hearts, Card::Ten));
    model->pushCard(new Card(Card::Diamonds, Card::Seven));
    return model;
}

CardListModelTest::CardListModelTest() {
    qRegisterMetaType<QModelIndex>("QModelIndex");
}

void CardListModelTest::emptyList() {
    CardListModel model;
    QScopedPointer<ModelTest> testmodel(new ModelTest(&model));
    Q_UNUSED(testmodel);
    QScopedPointer<ModelObserver> observerModel(new ModelObserver(&model, CardListModel::SuitRole, CardListModel::ColorRole));
    QVERIFY(model.isEmpty());
    QCOMPARE(model.rowCount(), 0);
    observerModel->checkSynchronised();
}

void CardListModelTest::pushCard() {
    CardListModel model;
    QScopedPointer<ModelTest> testmodel(new ModelTest(&model));
    Q_UNUSED(testmodel);
    QScopedPointer<ModelObserver> observerModel(new ModelObserver(&model, CardListModel::SuitRole, CardListModel::ColorRole));

    model.pushCard(new Card(Card::Diamonds, Card::Queen));

    QVERIFY(!model.isEmpty());
    QCOMPARE(model.rowCount(), 1);
    observerModel->checkSynchronised();
}

void CardListModelTest::pushCards() {
    CardListModel model;
    QScopedPointer<ModelTest> testmodel(new ModelTest(&model));
    Q_UNUSED(testmodel);
    QScopedPointer<ModelObserver> observerModel(new ModelObserver(&model, CardListModel::SuitRole, CardListModel::ColorRole));

    model.pushCards(QList<Card*>() << new Card() << new Card() << new Card());

    QCOMPARE(model.rowCount(), 3);
    observerModel->checkSynchronised();
}

void CardListModelTest::popCard() {
    CardListModel model;
    QScopedPointer<ModelTest> testmodel(new ModelTest(&model));
    Q_UNUSED(testmodel);
    QScopedPointer<ModelObserver> observerModel(new ModelObserver(&model, CardListModel::SuitRole, CardListModel::ColorRole));

    model.pushCard(new Card());
    Card* card = model.popCard();

    QVERIFY(card);
    QCOMPARE(model.rowCount(), 0);
    observerModel->checkSynchronised();
}

void CardListModelTest::buildDeck32() {
    CardListModel model;
    QScopedPointer<ModelTest> testmodel(new ModelTest(&model));
    Q_UNUSED(testmodel);
    model.pushCard(new Card());
    model.buildDeck32();
    QCOMPARE(model.cardList().first()->isFlipped(), true);      // Cards should not be visible when building a deck
    QCOMPARE(model.cardList().last()->isFlipped(), true);      // Cards should not be visible when building a deck
    QCOMPARE(model.rowCount(), 32);
}

void CardListModelTest::buildDeck52() {
    CardListModel model;
    QScopedPointer<ModelTest> testmodel(new ModelTest(&model));
    Q_UNUSED(testmodel);
    model.pushCard(new Card());
    model.buildDeck52();
    QCOMPARE(model.cardList().first()->isFlipped(), true);      // Cards should not be visible when building a deck
    QCOMPARE(model.cardList().last()->isFlipped(), true);      // Cards should not be visible when building a deck
    QCOMPARE(model.rowCount(), 52);
}

void CardListModelTest::popLasts_data() {
    QTest::addColumn<int>("popCount");
    QTest::newRow("Zero") << 0;
    QTest::newRow("One") << 1;
    QTest::newRow("Three") << 3;
    QTest::newRow("Hundred") << 100;
}

void CardListModelTest::popLasts() {
    QFETCH(int, popCount);

    QScopedPointer<CardListModel> model(buildDeckModel());
    QScopedPointer<ModelTest> testmodel(new ModelTest(model.data()));
    Q_UNUSED(testmodel);
    QScopedPointer<ModelObserver> observerModel(new ModelObserver(model.data(), CardListModel::SuitRole, CardListModel::ColorRole));

    int modelStartCount = model->rowCount();
    QList<Card*> card = model->popLasts(popCount);
    popCount = qMin(modelStartCount, popCount);

    if (popCount > 0) {
        QCOMPARE(card.last()->getSuit(), Card::Diamonds);
        QCOMPARE(card.last()->getValue(), Card::Seven);
    }

    QCOMPARE(card.count(), popCount);
    QCOMPARE(model->rowCount(), modelStartCount - popCount);
    observerModel->checkSynchronised();
}

void CardListModelTest::takeAll() {
    QScopedPointer<CardListModel> model(buildDeckModel());
    QScopedPointer<ModelTest> testmodel(new ModelTest(model.data()));
    Q_UNUSED(testmodel);
    QScopedPointer<ModelObserver> observerModel(new ModelObserver(model.data(), CardListModel::SuitRole, CardListModel::ColorRole));

    int modelStartCount = model->rowCount();
    QList<Card*> list = model->takeAll();

    QCOMPARE(model->rowCount(), 0);
    QCOMPARE(list.count(), modelStartCount);
    observerModel->checkSynchronised();
}

void CardListModelTest::clear() {
    QScopedPointer<CardListModel> model(buildDeckModel());
    QScopedPointer<ModelTest> testmodel(new ModelTest(model.data()));
    Q_UNUSED(testmodel);
    QScopedPointer<ModelObserver> observerModel(new ModelObserver(model.data(), CardListModel::SuitRole, CardListModel::ColorRole));

    model->clear();

    QCOMPARE(model->rowCount(), 0);
    observerModel->checkSynchronised();
}

void CardListModelTest::hasCardEmpty() {
    CardListModel model;
    QVERIFY(model.contains(Card::Diamonds, Card::Ace) == false);
}

void CardListModelTest::hasCardValid() {
    QScopedPointer<CardListModel> model(buildDeckModel());
    model->pushCard(new Card(Card::Diamonds, Card::Queen));
    QVERIFY(model->contains(Card::Diamonds, Card::Queen));
}

void CardListModelTest::hasNotCard() {
    CardListModel model;
    model.pushCard(new Card(Card::Diamonds, Card::Queen));
    QVERIFY(model.contains(Card::Diamonds, Card::King) == false);
}

void CardListModelTest::getCard() {
    CardListModel model;
    Card* original = new Card(Card::Diamonds, Card::Queen);
    model.pushCard(original);
    const Card* invalidCard = model.getCard(Card::Spades, Card::Jack);
    const Card* validCard = model.getCard(Card::Diamonds, Card::Queen);

    QVERIFY(invalidCard == 00);
    QVERIFY(validCard != 00);
    QVERIFY(validCard->isValid());
    QVERIFY(validCard == original);
    QCOMPARE(validCard->getSuit(), original->getSuit());
    QCOMPARE(validCard->getValue(), validCard->getValue());
}

void CardListModelTest::setSelectable() {
    CardListModel model;
    QScopedPointer<ModelObserver> observerModel(new ModelObserver(&model, CardListModel::SuitRole, CardListModel::ColorRole));

    Card* original = new Card(Card::Diamonds, Card::Queen);
    model.pushCard(original);
    model.pushCard(new Card(Card::Hearts, Card::Jack));
    model.setCardSelectable(original, true);
    model.setCardSelectable(Card::Hearts, Card::Jack, true);
    model.setCardSelectable(Card::Spades, Card::Ace, true);
    const Card* card1 = model.getCard(Card::Diamonds, Card::Queen);
    const Card* card2 = model.getCard(Card::Hearts, Card::Jack);

    QVERIFY(card1->isSelectable());
    QVERIFY(card2->isSelectable());
    observerModel->checkSynchronised();

    model.setCardSelectable(original, false);
    model.setCardSelectable(Card::Hearts, Card::Jack, false);
    QVERIFY(!card1->isSelectable());
    QVERIFY(!card2->isSelectable());
    observerModel->checkSynchronised();
}

void CardListModelTest::setSelectableAlreadySelected()
{
    CardListModel model;

    Card* original = new Card(Card::Diamonds, Card::Queen);
    model.pushCard(original);
    model.setCardSelectable(original, true);

    QSignalSpy dataChangedSpy(&model, SIGNAL(dataChanged(QModelIndex,QModelIndex)));
    model.setCardSelectable(original, true);

    QCOMPARE(dataChangedSpy.count(), 0);
}

void CardListModelTest::setFlipped()
{
    CardListModel model;
    QScopedPointer<ModelObserver> observerModel(new ModelObserver(&model, CardListModel::SuitRole, CardListModel::ColorRole));
    Card* original = new Card(Card::Diamonds, Card::Queen);
    model.pushCard(original);
    model.pushCard(new Card(Card::Hearts, Card::Jack));
    model.setCardFlipped(original, true);
    model.setCardFlipped(Card::Hearts, Card::Jack, true);
    model.setCardFlipped(Card::Spades, Card::Ace, true);
    const Card* card1 = model.getCard(Card::Diamonds, Card::Queen);
    const Card* card2 = model.getCard(Card::Hearts, Card::Jack);

    QVERIFY(card1->isFlipped());
    QVERIFY(card2->isFlipped());
    observerModel->checkSynchronised();

    model.setCardFlipped(original, false);
    model.setCardFlipped(Card::Hearts, Card::Jack, false);
    QVERIFY(!card1->isFlipped());
    QVERIFY(!card2->isFlipped());
    observerModel->checkSynchronised();
}

void CardListModelTest::setFlippedAlreadyFlipped()
{
    CardListModel model;
    Card* original = new Card(Card::Diamonds, Card::Queen);
    model.pushCard(original);
    model.setCardFlipped(original, true);
    QSignalSpy dataChangedSpy(&model, SIGNAL(dataChanged(QModelIndex,QModelIndex)));
    model.setCardFlipped(original, true);

    QCOMPARE(dataChangedSpy.count(), 0);
}

void CardListModelTest::dataTest()
{
    CardListModel model;
    Card* original = new Card(Card::Diamonds, Card::Queen);
    model.pushCard(original);
    QCOMPARE((int)original->getValue(), model.data(model.index(0, 0), CardListModel::ValueRole).toInt());
    QCOMPARE((int)original->getSuit(), model.data(model.index(0, 0), CardListModel::SuitRole).toInt());
    QVERIFY(original->isFlipped() == model.data(model.index(0, 0), CardListModel::FlippedRole).toBool());
    QVERIFY(original->isSelectable() == model.data(model.index(0, 0), CardListModel::SelectableRole).toBool());
    QCOMPARE((int)original->getColor(), model.data(model.index(0, 0), CardListModel::ColorRole).toInt());
}

void CardListModelTest::setAllFlipped()
{
    QScopedPointer<CardListModel> model(buildDeckModel());
    Card* original = new Card(Card::Diamonds, Card::Queen);
    original->flip();
    model->pushCard(original);
    model->setAllFlipped(true);
    QVERIFY(model->data(model->index(0, 0), CardListModel::FlippedRole).toBool() == true);
    QVERIFY(model->data(model->index(model->rowCount() / 2, 0), CardListModel::FlippedRole).toBool() == true);
    QVERIFY(model->data(model->index(model->rowCount() - 1, 0), CardListModel::FlippedRole).toBool() == true);
}

void CardListModelTest::setAllSelectable()
{
    QScopedPointer<CardListModel> model(buildDeckModel());
    Card* original = new Card(Card::Diamonds, Card::Queen);
    original->setSelectable(true);
    model->pushCard(original);
    model->setAllSelectable(true);
    QVERIFY(model->data(model->index(0, 0), CardListModel::SelectableRole).toBool() == true);
    QVERIFY(model->data(model->index(model->rowCount() / 2, 0), CardListModel::SelectableRole).toBool() == true);
    QVERIFY(model->data(model->index(model->rowCount() - 1, 0), CardListModel::SelectableRole).toBool() == true);
}

void CardListModelTest::constCardList()
{
    CardListModel model;
    model.pushCards(QList<Card*>() << new Card(Card::Diamonds, Card::Ten) <<
                    new Card(Card::Spades, Card::Queen) << new Card(Card::Diamonds, Card::King));
    QList<const Card*> constList = model.cardList();

    QCOMPARE(constList.size(), model.rowCount());
    QCOMPARE(constList.first()->getValue(), Card::Ten);
    QCOMPARE(constList.first()->getSuit(), Card::Diamonds);
    QCOMPARE(constList.last()->getValue(), Card::King);
    QCOMPARE(constList.last()->getSuit(), Card::Diamonds);
}

