#include "NLastProxyModelTest.h"

#include <QtTest>

#include "../Patience/Card.h"
#include "../Patience/NLastProxyModel.h"
#include "../Patience/CardListModel.h"
#include "ModelTest.h"
#include "ModelObserver.h"

NLastProxyModelTest::NLastProxyModelTest(QObject *parent) :
    QObject(parent)
{
}

void NLastProxyModelTest::staticDataTest()
{
    CardListModel source;
    NLastProxyModel proxy(3);
    proxy.setSourceModel(&source);
    QScopedPointer<ModelTest> testmodel(new ModelTest(&proxy));
    Q_UNUSED(testmodel);

    QCOMPARE(proxy.rowCount(), 0);

    source.pushCard(new Card(Card::Diamonds, Card::Four));
    QCOMPARE(proxy.rowCount(), 1);
    QCOMPARE(proxy.data(proxy.index(0), CardModel::ValueRole).toInt(), (int)Card::Four);

    source.pushCard(new Card(Card::Clubs, Card::Ace));
    source.pushCard(new Card(Card::Spades, Card::King));
    QCOMPARE(proxy.rowCount(), 3);
    QCOMPARE(proxy.data(proxy.index(0), CardModel::ValueRole).toInt(), (int)Card::Four);
    QCOMPARE(proxy.data(proxy.index(2), CardModel::ValueRole).toInt(), (int)Card::King);

    source.pushCard(new Card(Card::Hearts, Card::Ten));
    source.pushCard(new Card(Card::Diamonds, Card::Seven));
    QCOMPARE(proxy.rowCount(), 3);
    QCOMPARE(proxy.data(proxy.index(0), CardModel::ValueRole).toInt(), (int)Card::King);
    QCOMPARE(proxy.data(proxy.index(2), CardModel::ValueRole).toInt(), (int)Card::Seven);
}

void NLastProxyModelTest::pushDataTest()
{
    CardListModel source;
    NLastProxyModel proxy(3);
    proxy.setSourceModel(&source);
    QScopedPointer<ModelObserver> observerModel(new ModelObserver(&proxy, Qt::UserRole + 1, CardModel::LastRole - 1));

    source.pushCard(new Card(Card::Diamonds, Card::Four));

    observerModel->checkSynchronised();
}

void NLastProxyModelTest::popDataTest()
{
    CardListModel source;
    NLastProxyModel proxy(3);
    proxy.setSourceModel(&source);
    QScopedPointer<ModelObserver> observerModel(new ModelObserver(&proxy, Qt::UserRole + 1, CardModel::LastRole - 1));

    source.pushCard(new Card(Card::Clubs, Card::Ace));
    source.pushCard(new Card(Card::Spades, Card::King));
    source.pushCard(new Card(Card::Diamonds, Card::Four));

    observerModel->checkSynchronised();

    source.popCard();
    observerModel->checkSynchronised();
}

void NLastProxyModelTest::clearDataTest()
{
    CardListModel source;
    NLastProxyModel proxy(3);
    proxy.setSourceModel(&source);
    QScopedPointer<ModelObserver> observerModel(new ModelObserver(&proxy, Qt::UserRole + 1, CardModel::LastRole - 1));

    source.pushCard(new Card(Card::Diamonds, Card::Four));
    source.pushCard(new Card(Card::Spades, Card::King));
    source.pushCard(new Card(Card::Diamonds, Card::Four));

    observerModel->checkSynchronised();

    source.takeAll();
    observerModel->checkSynchronised();
}

void NLastProxyModelTest::dataModifiedTest()
{
    CardListModel source;
    NLastProxyModel proxy(3);
    proxy.setSourceModel(&source);
    QScopedPointer<ModelObserver> observerModel(new ModelObserver(&proxy, Qt::UserRole + 1, CardModel::LastRole - 1));

    source.pushCard(new Card(Card::Diamonds, Card::Four));
    source.pushCard(new Card(Card::Spades, Card::King));

    observerModel->checkSynchronised();

    source.setCardFlipped(Card::Spades, Card::King, false);
    source.setCardFlipped(Card::Diamonds, Card::Four, true);
    observerModel->checkSynchronised();
}
