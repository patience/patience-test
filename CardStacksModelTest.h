#ifndef CARDSTACKMODELTEST_H
#define CARDSTACKMODELTEST_H

#include <QObject>

class CardStacksModel;

class CardStacksModelTest : public QObject
{
    Q_OBJECT
public:
    explicit CardStacksModelTest(QObject *parent = 0);

private Q_SLOTS:
    void emptyList();
    void pushCard();
    void pushCards();
    void popCard();
    void popLasts_data();
    void popLasts();
    void takeAll();
    void clear();
    void clearAll();
    void hasCardEmpty();
    void hasCardValid();
    void hasNotCard();
    void getCard();
    void setSelectable();
    void setSelectableAlreadySelected();
    void setFlipped();
    void setFlippedAlreadyFlipped();
    void dataTest();
    void setAllSelectable();
    void setAllFlipped();
    void constCardList();
    void emptyStacks();

private:
    void addCardsToModel(CardStacksModel* model, int stackIndex = 0);
};

#endif // CARDSTACKMODELTEST_H
