#include "CardTest.h"

#include <QtCore/QString>
#include <QtTest/QtTest>
#include <QSignalSpy>

#include "../Patience/Card.h"

CardTest::CardTest()
{
}

void CardTest::init() {
    tenDiamonds = new Card(Card::Diamonds, Card::Ten);
    tenSpades = new Card(Card::Spades, Card::Ten);
}

void CardTest::cleanup() {
    delete tenDiamonds;
    delete tenSpades;
}

void CardTest::invalidBuild() {
    Card* invalid = new Card();
    QVERIFY(invalid->isValid() == false);
}

void CardTest::redColorBuild()
{
    QVERIFY(tenDiamonds->getColor() == Card::Red);
}

void CardTest::blackColorBuild()
{
    QVERIFY(tenSpades->getColor() == Card::Black);
}

void CardTest::flippedBuild() {
    QVERIFY(tenSpades->isFlipped() == false);
}

void CardTest::flipCard() {
    QSignalSpy spy(tenSpades, SIGNAL(flippedChanged(bool)));
    tenSpades->flip();
    QVERIFY(tenSpades->isFlipped() == true);
    QCOMPARE(spy.count(), 1);
}

void CardTest::selectableBuild() {
    QVERIFY(tenSpades->isSelectable() == false);
}

void CardTest::setFlippedCard() {
    QSignalSpy spy(tenSpades, SIGNAL(flippedChanged(bool)));
    tenSpades->setFlipped(true);
    QVERIFY(tenSpades->isFlipped() == true);
    QCOMPARE(spy.count(), 1);
}

void CardTest::setSelectable() {
    QSignalSpy spy(tenSpades, SIGNAL(selectableChanged(bool)));
    tenSpades->setSelectable(true);
    QVERIFY(tenSpades->isSelectable() == true);
    QCOMPARE(spy.count(), 1);
}
