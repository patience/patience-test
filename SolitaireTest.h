#ifndef SOLITAIRETEST_H
#define SOLITAIRETEST_H

#include <QObject>

class Solitaire;

class SolitaireTest : public QObject
{
    Q_OBJECT
public:
    explicit SolitaireTest(QObject *parent = 0);

signals:

private slots:
    void newgameTest();
    void drawDeckTest();
    void solitaireMoveTest();
    void discardToSolitaireTest();
    void discardToAcesTest();
    void solitaireToAcesTest();

private:
    Solitaire* buildSolitaire();
};

#endif // SOLITAIRETEST_H
