#include "CardTest.h"
#include "CardListModelTest.h"
#include "CardStacksModelTest.h"
#include "NLastProxyModelTest.h"
#include "SolitaireTest.h"

#include <QTest>
using namespace QTest;

int main(int argc, char *argv[])
{
    Q_UNUSED(argc);
    Q_UNUSED(argv);

    qExec(new CardTest());
    qExec(new CardListModelTest());
    qExec(new CardStacksModelTest());
    qExec(new NLastProxyModelTest());
    qExec(new SolitaireTest());
}

